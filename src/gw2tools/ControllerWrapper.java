/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gw2tools;

/**
 *
 * @author Philipp
 */
public class ControllerWrapper {
    private static ControllerWrapper instance;
    private static FXMLDocumentController controller;
    
    private ControllerWrapper(FXMLDocumentController ctrl){
        ControllerWrapper.controller = ctrl;
    }    
    
    public static ControllerWrapper getInstance(FXMLDocumentController con){
        if (ControllerWrapper.instance == null){
            ControllerWrapper.instance = new ControllerWrapper(con);
        }
        return ControllerWrapper.instance;
    }
    
    public static FXMLDocumentController getController(){
        return ControllerWrapper.controller;
    }
}
