/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gw2tools;

import gw2tools.GW2API.GW2;
import gw2tools.GW2API.GW2IconRenderer;
import gw2tools.GW2API.GW2Item;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javax.swing.JOptionPane;

/**
 *
 * @author Philipp
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Button createLocalDBButton;
    @FXML
    private Button searchButton;
    @FXML
    private CheckBox useLocalDB;
    @FXML
    private TextField searchField;
    @FXML
    private ImageView iconView;
    @FXML
    private TextArea infoArea;
    @FXML
    private Label localDBexistsLabel;
    @FXML
    private Label itemNameLabel;
    @FXML
    private AnchorPane tableAnchor;
    @FXML
    private CheckBox specificSearch;
    @FXML
    private Button createRecipeDB;

    private TableView resultsTable;
    private Integer threadCount = 1000;
    private final String dbPath = System.getProperty("user.dir") + "\\databases";
    private Boolean localDB = true;
    private Integer actualProgress = 0;
    private ArrayList<GW2Item> searchResults;

    public void showItemDetails(GW2Item item) throws Exception {
        itemNameLabel.setText(item.getItem_name());
        infoArea.clear();
        infoArea.appendText(item.toString());
        iconView.setImage(GW2IconRenderer.renderImage(item.getItem_icon_id(), item.getItem_file_signature(), ".png"));
    }

    @FXML
    private void handleSearchButton(ActionEvent e) throws IOException, Exception {
        GW2.useLocalDatabase(true);
        String search = searchField.getText();
        if (search.indexOf("=") != -1) {
            String sattr = search.substring(0, search.indexOf("="));
            search = search.substring(search.indexOf("=") + 1, search.length());
            searchResults = GW2.searchItem(new String[]{sattr}, new String[]{search}, !specificSearch.isSelected());
        } else {
            searchResults = GW2.searchItem(new String[]{"item_name"}, new String[]{searchField.getText()}, !specificSearch.isSelected());
        }
        if (searchResults != null) {
            resultsTable = createResultsTable(searchResults);
            resultsTable.setPrefHeight(tableAnchor.heightProperty().get());
            tableAnchor.getChildren().add(resultsTable);
        } else {
            System.out.println("nothing found");
        }
    }

    private TableView createResultsTable(ArrayList<GW2Item> arr) {
        ObservableList<GW2Item> data = FXCollections.observableArrayList();
        for (GW2Item item : arr) {
            data.add(item);
        }

        TableColumn<GW2Item, String> idCol = new TableColumn<>("Item ID");
        idCol.setCellValueFactory(new PropertyValueFactory<GW2Item, String>("itemID"));

        TableColumn<GW2Item, String> nameCol = new TableColumn<>("Item Name");
        nameCol.setCellValueFactory(new PropertyValueFactory<GW2Item, String>("itemName"));

        TableView<GW2Item> results = new TableView<>();
        results.setItems(data);

        results.getColumns().addAll(idCol, nameCol);

        idCol.setPrefWidth(100);
        nameCol.setPrefWidth(200);
        
        results.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Integer selected = resultsTable.getSelectionModel().getSelectedIndex();
                try {
                    showItemDetails(searchResults.get(selected));
                } catch (Exception ex) {
                    Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        return results;
    }

    @FXML
    private void handleCreateDBButton(ActionEvent e) throws IOException, InterruptedException, Exception {
        int response = JOptionPane.showConfirmDialog(null,
                "This may several Minutes! \n Continue?",
                "Warning",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null);
        if (response == JOptionPane.YES_OPTION) {
            GW2.getAllItems(1000);
            GW2.addToLocalDatabase(GW2.ITEM_DATA);
        }
    }

    @FXML
    private void handleUseLocalDBCheckBox(ActionEvent e) {
        searchResults = new ArrayList<>();
        localDB = useLocalDB.isSelected();
        if (localDB) {
            File test = new File(dbPath + "\\itemData.ini");
            if (test.exists()) {
                localDBexistsLabel.setVisible(false);
            }
        }

    }

    @FXML
    private void handleSearchField(KeyEvent ke) throws Exception{
        if (ke.getCode() == KeyCode.ENTER){
            handleSearchButton(new ActionEvent());
        }
    }
    
    @FXML
    private void handleCreateRecipeDB(ActionEvent e) throws IOException, Exception{
        int response = JOptionPane.showConfirmDialog(null,
                "This may several Minutes! \n Continue?",
                "Warning",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null);
        if (response == JOptionPane.YES_OPTION) {
            GW2.getAllRecipes(590);
            GW2.addToLocalDatabase(GW2.RECIPE_DATA);
        }
    }
    
    /*@FXML
     private void handleTestButton(ActionEvent e) throws IOException, InterruptedException {
     GW2.threadInitialiser(38377, Integer.valueOf(renderIconField.getText()));
     }
    
     @FXML
     private void handleRenderButton(ActionEvent e) throws IOException, Exception{
     GW2Item item = GW2.getItem(renderIconField.getText());
     String sign = item.getItem_file_signature();
     Integer id = item.getItem_icon_id();
     Image temp = GW2IconRenderer.renderImage(id.toString(), sign, ".png");
     iconView.setImage(temp);
     }
    
     @FXML
     private void handleGetItemButtton(ActionEvent e) throws IOException{
     GW2Item testItem = GW2.getItem("38875");
     }*/
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ControllerWrapper.getInstance(this);
        GW2.getInstance();
        GW2IconRenderer.getInstance();
        resultsTable = new TableView();

        File test = new File(dbPath + "\\gw2.sqlite");
        if (!test.exists()) {
            localDBexistsLabel.setTextFill(Paint.valueOf("red"));
            localDBexistsLabel.setFont(Font.font(9));
            localDBexistsLabel.setText("(no local DB found!)");
            localDBexistsLabel.setVisible(true);
            GW2.useLocalDatabase(true);
        }        
    }

}
