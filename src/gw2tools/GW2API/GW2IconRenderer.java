/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gw2tools.GW2API;

import javafx.scene.image.Image;

/**
 *
 * @author Philipp
 */
public class GW2IconRenderer {
    private static GW2IconRenderer instance;
    public static final String render_base_url = "https://render.guildwars2.com/file/";
    
    private GW2IconRenderer(){
        super();
    }
    
    public static GW2IconRenderer getInstance(){
        if (GW2IconRenderer.instance == null){
            GW2IconRenderer.instance = new GW2IconRenderer();
        }
        return GW2IconRenderer.instance;
    }
    
    public static Image renderImage(String file_id, String signature, String fType) throws Exception{
        if (fType != ".jpg" && fType != ".png"){
            throw new Exception("FileType not suported!");
        }
        String url = render_base_url+signature+"/"+file_id+fType;
        Image result = new Image(url);
        return result;
    }
    
    public static Image renderImage(String item_id) throws Exception{
        GW2Item item = GW2.getItem(item_id);
        String sign = item.getItem_file_signature();
        String id = item.getItem_icon_id();       
        Image result = GW2IconRenderer.renderImage(id, sign, ".png");
        return result;
    }
}
