/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gw2tools.GW2API;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Philipp
 */
public class GW2RecipeRequestThread extends Thread{
    private List<String> ids;
    private Integer threadId;
    private GW2Recipe temp_recipe = null;
    
    public GW2RecipeRequestThread(List<String> ids, Integer id){
        threadId = id;
        this.ids = ids;
    }
    
    private GW2Recipe getRecipe(String id){
        try{
            GW2Recipe recipe = GW2.getRecipe(id);
            return recipe;
        }catch(IOException e) {
            return this.getRecipe(id);
        }            
    }
    
    @Override
    public void run(){
        for (String elem : ids){
            try{
                temp_recipe = getRecipe(elem);
            }catch(Exception e){
                Logger.getLogger(GW2ItemRequestThread.class.getName()).log(Level.SEVERE, null, e);
            }
            GW2.getInstance().addThreadResult(temp_recipe);
        }
        GW2.getInstance().signalFinishedThread();
    }
}
