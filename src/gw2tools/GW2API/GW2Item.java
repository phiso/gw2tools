/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gw2tools.GW2API;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import gw2tools.ControllerWrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.beans.property.SimpleStringProperty;
import static javax.management.Query.attr;

/**
 *
 * @author Philipp
 */
public class GW2Item {

    private String item_id;
    private String item_name;
    private String item_descr;
    private String item_type;
    private String item_level;
    private String item_rarity;
    private String item_vendor_value;
    private String item_icon_id;
    private String item_file_signature;
    
    //needed for compatibility with FX ListView
    private final SimpleStringProperty itemID;
    private final SimpleStringProperty itemName;

    private ArrayList<String> item_gametypes;
    private ArrayList<String> item_flags;
    private ArrayList<String> item_restrictions;
    private HashMap<String, String> item_infix_attributes;
    private HashMap<String, String> item_attributes;
    private Integer arrCounter = 0;

    public GW2Item() {
        item_gametypes = new ArrayList<>();
        item_flags = new ArrayList<>();
        item_restrictions = new ArrayList<>();
        item_infix_attributes = new HashMap<>();
        item_attributes = new HashMap<>();
        item_id = "";
        item_name = "";
        item_descr = "";
        item_type = "";
        item_level = "";
        item_rarity = "";
        item_vendor_value = "";
        item_icon_id = "";
        item_file_signature = "";
        itemID = new SimpleStringProperty("");
        itemName = new SimpleStringProperty("");
    }

    public GW2Item(JsonObject jObj) throws Exception {
        item_gametypes = new ArrayList<>();
        item_flags = new ArrayList<>();
        item_restrictions = new ArrayList<>();
        item_infix_attributes = new HashMap<>();
        item_attributes = new HashMap<>();

        item_id = jObj.get("item_id").getAsString();
        item_name = jObj.get("name").getAsString();
        if (jObj.get("description") != null) {
            item_descr = jObj.get("description").getAsString();
        } else {
            item_descr = "None";
        }
        item_type = jObj.get("type").getAsString();
        item_level = jObj.get("level").getAsString();
        item_rarity = jObj.get("rarity").getAsString();
        item_vendor_value = jObj.get("vendor_value").getAsString();
        item_icon_id = jObj.get("icon_file_id").getAsString();
        item_file_signature = jObj.get("icon_file_signature").getAsString();
        
        itemID = new SimpleStringProperty(item_id);
        itemName = new SimpleStringProperty(item_name);

        JsonArray tempArr = jObj.getAsJsonArray("game_types");
        for (int i = 0; i < tempArr.size(); i++) {
            item_gametypes.add(tempArr.get(i).getAsString());
        }
        tempArr = jObj.getAsJsonArray("flags");
        for (int i = 0; i < tempArr.size(); i++) {
            item_flags.add(tempArr.get(i).getAsString());
        }
        tempArr = jObj.getAsJsonArray("restrictions");
        for (int i = 0; i < tempArr.size(); i++) {
            item_restrictions.add(tempArr.get(i).getAsString());
        }

        JsonObject attr = null;
        if (item_type.equals("UpgradeComponent")) {
            attr = jObj.getAsJsonObject("upgrade_component");
        } else {
            attr = jObj.getAsJsonObject(item_type.toLowerCase());
        }
        if (attr != null && attr.isJsonObject()) {
            for (Map.Entry<String, JsonElement> entry : attr.entrySet()) {
                arrCounter = 0;
                if (entry.getValue().isJsonObject()) {
                    getJsonObject(entry.getValue().getAsJsonObject(), entry.getKey());
                } else if (entry.getValue().isJsonArray()) {
                    getJsonArray(entry.getValue().getAsJsonArray(), entry.getKey());
                } else if (entry.getValue().isJsonPrimitive()) {
                    item_attributes.put(entry.getKey(), entry.getValue().getAsString());
                } else {
                    throw new Exception("Error while reading Item Attributes (" + entry.getKey() + " - " + entry.getValue().getAsString() + ")");
                }
            }
        }
    }

    private void getJsonObject(JsonObject obj, String name) {
        for (Map.Entry<String, JsonElement> entry : obj.entrySet()) {
            if (entry.getValue().isJsonPrimitive()) {
                item_attributes.put(name + "|" + arrCounter, entry.getKey() + "|" + entry.getValue().getAsString());
                arrCounter++;
            } else if (entry.getValue().isJsonObject()) {
                getJsonObject(entry.getValue().getAsJsonObject(), name);
            } else if (entry.getValue().isJsonArray()) {
                getJsonArray(entry.getValue().getAsJsonArray(), name);
            }
        }
    }

    private void getJsonArray(JsonArray arr, String name) {
        for (int i = 0; i < arr.size(); i++) {
            if (arr.get(i).isJsonPrimitive()) {
                item_attributes.put(name + "|" + arrCounter, arr.get(i).getAsString());
                arrCounter++;
            } else if (arr.get(i).isJsonArray()) {
                getJsonArray(arr.get(i).getAsJsonArray(), name);
            } else if (arr.get(i).isJsonObject()) {
                getJsonObject(arr.get(i).getAsJsonObject(), name);
            }
        }
    }

    /**
     * @return the item_id
     */
    public String getItem_id() {
        return item_id;
    }

    /**
     * @return the item_name
     */
    public String getItem_name() {
        return item_name;
    }

    /**
     * @return the item_descr
     */
    public String getItem_descr() {
        return item_descr;
    }

    /**
     * @return the item_type
     */
    public String getItem_type() {
        return item_type;
    }

    /**
     * @return the item_level
     */
    public String getItem_level() {
        return item_level;
    }

    /**
     * @return the item_rarity
     */
    public String getItem_rarity() {
        return item_rarity;
    }

    /**
     * @return the item_vendor_value
     */
    public String getItem_vendor_value() {
        return item_vendor_value;
    }

    /**
     * @return the item_icon_id
     */
    public String getItem_icon_id() {
        return item_icon_id;
    }

    /**
     * @return the item_file_signature
     */
    public String getItem_file_signature() {
        return item_file_signature;
    }

    /**
     * @return the item_gametypes
     */
    public ArrayList<String> getItem_gametypes() {
        return item_gametypes;
    }

    /**
     * @return the item_flags
     */
    public ArrayList<String> getItem_flags() {
        return item_flags;
    }

    /**
     * @return the item_restrictions
     */
    public ArrayList<String> getItem_restrictions() {
        return item_restrictions;
    }

    /**
     * @return the item_infix_attributes
     */
    public HashMap<String, String> getItem_infix_attributes() {
        return item_infix_attributes;
    }

    public HashMap<String, String> getItem_attributes() {
        return item_attributes;
    }
    
    public SimpleStringProperty getItemIDProperty(){
        return itemID;
    }
    
    public SimpleStringProperty getItemNameProperty(){
        return itemName;
    }
    
    public String getItemID(){
        return itemID.get();
    }
    
    public String getItemName(){
        return itemName.get();
    }

    /**
     * @param item_id the item_id to set
     */
    public void setItem_id(String item_id) {
        this.item_id = item_id;
        this.itemID.set(item_id);
    }

    /**
     * @param item_name the item_name to set
     */
    public void setItem_name(String item_name) {
        this.item_name = item_name;
        this.itemName.set(item_name);
    }

    /**
     * @param item_descr the item_descr to set
     */
    public void setItem_descr(String item_descr) {
        this.item_descr = item_descr;
    }

    /**
     * @param item_type the item_type to set
     */
    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }

    /**
     * @param item_level the item_level to set
     */
    public void setItem_level(String item_level) {
        this.item_level = item_level;
    }

    /**
     * @param item_rarity the item_rarity to set
     */
    public void setItem_rarity(String item_rarity) {
        this.item_rarity = item_rarity;
    }

    /**
     * @param item_vendor_value the item_vendor_value to set
     */
    public void setItem_vendor_value(String item_vendor_value) {
        this.item_vendor_value = item_vendor_value;
    }

    /**
     * @param item_icon_id the item_icon_id to set
     */
    public void setItem_icon_id(String item_icon_id) {
        this.item_icon_id = item_icon_id;
    }

    /**
     * @param item_file_signature the item_file_signature to set
     */
    public void setItem_file_signature(String item_file_signature) {
        this.item_file_signature = item_file_signature;
    }

    /**
     * @param defense the defense to set
     */
    /**
     * @param item_gametypes the item_gametypes to set
     */
    public void setItem_gametypes(ArrayList<String> item_gametypes) {
        this.item_gametypes = item_gametypes;
    }

    /**
     * @param item_flags the item_flags to set
     */
    public void setItem_flags(ArrayList<String> item_flags) {
        this.item_flags = item_flags;
    }

    /**
     * @param item_restrictions the item_restrictions to set
     */
    public void setItem_restrictions(ArrayList<String> item_restrictions) {
        this.item_restrictions = item_restrictions;
    }

    /**
     * @param item_infix_attributes the item_infix_attributes to set
     */
    public void setItem_infix_attributes(HashMap<String, String> item_infix_attributes) {
        this.item_infix_attributes = item_infix_attributes;
    }

    public void setItem_attributes(HashMap<String, String> attributes) {
        this.item_attributes = (HashMap<String, String>) attributes.clone();
    }

    @Override
    public String toString() {
        String result = "";
        result += "Item_id = " + this.item_id + "\n";
        result += "Item_name = " + this.item_name + "\n";
        result += "Item_type = "+this.item_type+"\n";
        result += "Item_descr = " + this.item_descr + "\n";
        result += "Item_level = " + this.item_level + "\n";
        result += "Item_rarity = " + this.item_rarity + "\n";
        result += "Item_vendor_value = " + this.item_vendor_value + "\n";
        result += "Item_icon_id = " + this.item_icon_id + "\n";
        result += "Item_icon_signature = " + this.item_file_signature + "\n";
        result += "Item_flags = " + this.item_flags.toString() + "\n";
        result += "Item_gametypes = " + this.item_gametypes.toString() + "\n";
        result += "Item_restrictions = " + this.item_restrictions.toString() + "\n";
        for (String key : this.item_attributes.keySet()){
            result += key+" = "+this.item_attributes.get(key)+"\n";
        }
        return result;
    }        
}
