/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gw2tools.GW2API;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.ArrayList;

/**
 *
 * @author Philipp
 */
public class GW2Map {

    public final String map_name;
    public final Integer min_level;
    public final Integer max_level;
    public final Integer default_floor;
    public final ArrayList<Integer> floors;
    public final Integer region_id;
    public final String region_name;
    public final Integer continent_id;
    public final String continent_name;
    public final ArrayList<Integer> map_rect;
    public final ArrayList<Integer> continent_rect;

    public GW2Map(JsonObject obj) {
        map_name = obj.get("map_name").getAsString();
        min_level = obj.get("min__level").getAsInt();
        max_level = obj.get("max_level").getAsInt();
        default_floor = obj.get("default_floor").getAsInt();
        floors = new ArrayList<>();
        JsonArray arr = obj.getAsJsonArray("floors");
        for (int i = 0; i < arr.size(); i++) {
            floors.add(arr.get(i).getAsInt());
        }
        region_id = obj.get("region_id").getAsInt();
        region_name = obj.get("region_name").getAsString();
        continent_id = obj.get("continent_id").getAsInt();
        continent_name = obj.get("continent_name").getAsString();
        
        map_rect = new ArrayList<>();
        arr = obj.getAsJsonArray("map_rect");
        for (int i = 0; i < arr.size(); i++) {
            map_rect.add(arr.get(i).getAsInt());
        }
        
        continent_rect = new ArrayList<>();
        arr = obj.getAsJsonArray("continent_rect");
        for (int i = 0; i < arr.size(); i++) {
            continent_rect.add(arr.get(i).getAsInt());
        }
    }
}
