/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gw2tools.GW2API;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Philipp
 */
public class GW2SQLiteWrapper {

    private String db_path;
    private Connection connection;

    public GW2SQLiteWrapper() {
        connection = null;
    }

    public void openDB(String path) throws Exception {
        this.db_path = path;
        Class.forName("org.sqlite.JDBC");
        File f = new File(path);
        if (f.exists() && !f.isDirectory()) {
            connection = DriverManager.getConnection("jdbc:sqlite:" + path);
            connection.setAutoCommit(false);
            System.out.println("Database found and Connected");
        } else {
            throw new Exception(path + " - No Database found!");
        }
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }

    public void addGW2ItemList(ArrayList<GW2Item> itemList) throws SQLException {
        PreparedStatement stmt = null;
        String queryTemplate = "insert into items (item_id, item_name, item_description, item_level, "
                + "item_rarity, item_vendor_value, item_icon_id, item_icon_signature, "
                + "item_flags, item_restrictions, item_gametypes, item_type) values (?,?,?,?,?,?,?,?,?,?,?,?)";
        stmt = connection.prepareStatement(queryTemplate);
        System.out.println(new java.util.Date());
        for (int i = 0; i < itemList.size(); i++) {
            GW2Item item = itemList.get(i);
            stmt.setInt(1, Integer.valueOf(item.getItem_id()));
            stmt.setString(2, item.getItem_name());
            stmt.setString(3, item.getItem_descr());
            stmt.setInt(4, Integer.valueOf(item.getItem_level()));
            stmt.setString(5, item.getItem_rarity());
            stmt.setInt(6, Integer.valueOf(item.getItem_vendor_value()));
            stmt.setInt(7, Integer.valueOf(item.getItem_icon_id()));
            stmt.setString(8, item.getItem_file_signature());
            stmt.setString(9, item.getItem_flags().toString());
            stmt.setString(10, item.getItem_restrictions().toString());
            stmt.setString(11, item.getItem_gametypes().toString());
            stmt.setString(12, item.getItem_type());

            String innerQuery = "insert into item_attributes (item_id,item_attribute_name,item_attribute_value)"
                    + " values (?,?,?)";
            String elem_name = "";
            String elem_value = "";
            String keyStr = "";
            Integer keyNum = 0;
            PreparedStatement sub = null;
            sub = connection.prepareStatement(innerQuery);

            for (String key : item.getItem_attributes().keySet()) {
                sub.setInt(1, Integer.valueOf(item.getItem_id()));
                elem_value = item.getItem_attributes().get(key);
                //get elem_name
                if (key.indexOf("|") != -1) {
                    keyStr = key.substring(0, key.indexOf("|"));
                    keyNum = Integer.valueOf(key.substring(key.indexOf("|") + 1, key.length()));
                    if (elem_value.indexOf("|") != -1) {
                        String value_name = elem_value.substring(0, elem_value.indexOf("|"));
                        String value = elem_value.substring(elem_value.indexOf("|") + 1, elem_value.length());
                        if (value_name.equals("attribute")) {
                            sub.setString(2, value);
                            elem_value = item.getItem_attributes().get(keyStr + "|" + (keyNum + 1));
                            sub.setString(3, elem_value.substring(elem_value.indexOf("|") + 1, elem_value.length()));
                        } else {
                            sub.setString(2, value_name);
                            sub.setString(3, value);
                        }
                    } else {
                        sub.setString(2, keyStr);
                        sub.setString(3, elem_value);
                    }
                } else {
                    sub.setString(2, key);
                    sub.setString(3, elem_value);
                }
                sub.addBatch();
            }
            sub.executeBatch();
            stmt.addBatch();
            System.out.println("added Database entry " + i);
            if ((i % 1000) == 0) {
                stmt.executeBatch();
            }
        }
        stmt.executeBatch();
        connection.commit();
        System.out.println("Finished (items): " + new java.util.Date());
    }

    public void addGW2RecipeList(ArrayList<GW2Recipe> recipes) throws SQLException {
        PreparedStatement stmt = null;
        String queryTemplate = "insert into recipes (recipe_id,recipe_type,recipe_output_id,"
                + "recipe_output_count,recipe_disciplines,recipe_min_rating,recipe_flags,"
                + "recipe_timetocraft) values (?,?,?,?,?,?,?,?)";
        stmt = connection.prepareStatement(queryTemplate);
        System.out.println(new java.util.Date());
        for (int i = 0; i < recipes.size(); i++) {
            GW2Recipe recipe = recipes.get(i);
            stmt.setInt(1, Integer.valueOf(recipe.getRecipe_id()));
            stmt.setString(2, recipe.getRecipe_type());
            stmt.setInt(3, Integer.valueOf(recipe.getRecipe_output_item()));
            stmt.setInt(4, Integer.valueOf(recipe.getRecipe_output_item_count()));
            stmt.setString(5, recipe.getRecipe_disciplines().toString());
            stmt.setInt(6, Integer.valueOf(recipe.getRecipe_minRating()));
            stmt.setString(7, recipe.getRecipe_flags().toString());
            stmt.setInt(8, Integer.valueOf(recipe.getRecipe_craftTime()));
            
            String innerQuery = "insert into recipe_ingredients (recipe_id,ingredient_id,ingredient_count,"
                    + "ingredient_isrecipeid) values (?,?,?,?)";
            PreparedStatement sub = null;
            sub = connection.prepareStatement(innerQuery);
            for (String key : recipe.getRecipe_ingredients().keySet()){
                sub.setInt(1, Integer.valueOf(recipe.getRecipe_id()));
                sub.setInt(2, Integer.valueOf(key));
                sub.setInt(3, Integer.valueOf(recipe.getRecipe_ingredients().get(key)));
                sub.setBoolean(4, false);
                sub.addBatch();
            }
            sub.executeBatch();
            stmt.addBatch();
            System.out.println("added Database entry " + i);
            if ((i % 1000) == 0){
                stmt.executeBatch();
            }
        }
        stmt.executeBatch();
        connection.commit();
        System.out.println("Finished (recipes): " + new java.util.Date());
    }

    public ArrayList<GW2Item> queryItems(String[] attr, String[] values, Boolean directSearch) throws Exception {
        ArrayList<GW2Item> result = new ArrayList<>();
        if ((attr.length > 0 && values.length > 0) && attr.length == values.length) {
            String query = "select * from items where ";
            for (int i = 0; i < attr.length; i++) {
                if (directSearch) {
                    query += attr[i] + " = '%" + values[i] + "%'";
                } else {
                    query += attr[i] + " like '%" + values[i] + "%'";
                }
                if (i < attr.length - 1) {
                    query += " and ";
                }
            }
            query += ";";
            ResultSet rs = sqlQuery(query);
            //System.out.println(rs.getFetchSize()+" items found!");
            result = parseResultSet(rs);
        } else {
            throw new Exception("Exception in SQL-Query wrong Arguments given");
        }
        return result;
    }

    private ArrayList<GW2Item> parseResultSet(ResultSet rs) throws SQLException {
        ArrayList<GW2Item> result = new ArrayList<>();
        while (rs.next()) {
            GW2Item temp = new GW2Item();
            temp.setItem_id(String.valueOf(rs.getInt("item_id")));
            temp.setItem_name(rs.getString("item_name"));
            temp.setItem_type(rs.getString("item_type"));
            temp.setItem_descr(rs.getString("item_description"));
            temp.setItem_level(String.valueOf(rs.getInt("item_level")));
            temp.setItem_vendor_value(String.valueOf(rs.getInt("item_vendor_value")));
            temp.setItem_rarity(rs.getString("item_rarity"));
            temp.setItem_icon_id(String.valueOf(rs.getInt("item_icon_id")));
            temp.setItem_file_signature(rs.getString("item_icon_signature"));

            String temp_flags = rs.getString("item_flags");
            temp_flags = temp_flags.trim();
            temp_flags = temp_flags.substring(1, temp_flags.length() - 1);
            String[] flags = temp_flags.split(",");
            temp.setItem_flags(new ArrayList<String>(Arrays.asList(flags)));

            String temp_restrictions = rs.getString("item_restrictions");
            temp_restrictions = temp_restrictions.trim();
            temp_restrictions = temp_restrictions.substring(1, temp_restrictions.length() - 1);
            String[] restrictions = temp_restrictions.split(",");
            temp.setItem_restrictions(new ArrayList<String>(Arrays.asList(restrictions)));

            String temp_gametypes = rs.getString("item_gametypes");
            temp_gametypes = temp_gametypes.trim();
            temp_gametypes = temp_gametypes.substring(1, temp_gametypes.length() - 1);
            String[] gametypes = temp_gametypes.split(",");
            temp.setItem_gametypes(new ArrayList<String>(Arrays.asList(gametypes)));

            String subQuery = "select * from item_attributes where item_id = " + temp.getItem_id() + ";";
            ResultSet attributes = sqlQuery(subQuery);
            HashMap<String, String> temp_attr = new HashMap<>();
            while (attributes.next()) {
                temp_attr.put(attributes.getString("item_attribute_name"), attributes.getString("item_attribute_value"));
            }
            temp.setItem_attributes(temp_attr);
            result.add(temp);
            System.out.println("searching Items (" + rs.getRow() + ")");
        }
        return result;
    }

    private ResultSet sqlQuery(String queryString) {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = connection.createStatement();
            if (queryString.contains("select")) {
                rs = stmt.executeQuery(queryString);
            } else {
                stmt.executeUpdate(queryString);
                stmt.close();
            }
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + " : " + e.getMessage());
        }
        return rs;
    }

    public Boolean isConnected() throws SQLException {
        try {
            if (connection.isClosed()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }
}
