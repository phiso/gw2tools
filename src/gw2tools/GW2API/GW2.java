/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gw2tools.GW2API;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import org.ini4j.Ini;

/**
 *
 * @author Philipp
 */
public class GW2 {

    public static final Integer ITEM_DATA = 1;
    public static final Integer RECIPE_DATA = 2;
    public static final Integer ICON_DATA = 3;
    public static final Integer EVENT_DATA = 4;
    public static final Integer MATCH_DATA = 5;

    public static String base_url = "https://api.guildwars2.com";
    private static GW2 instance;
    private static String langCode;
    private static ArrayList<GW2Item> allItems;
    private static ArrayList<GW2Recipe> allRecipes;
    private Integer runningThreads = 0;
    private static Boolean usingLocalData;
    private static Integer maxItems;
    private static Ini itemsIni;
    private static Ini item_mapIni;
    private static Ini recipeIni;
    private static Ini recipe_mapIni;
    private static GW2SQLiteWrapper sql;
    private static String db_path;

    private GW2(String lang) {
        langCode = lang;
        allItems = new ArrayList<>();
        allRecipes = new ArrayList<>();
        usingLocalData = false;
        sql = new GW2SQLiteWrapper();
        db_path = System.getProperty("user.dir") + "\\databases\\gw2.sqlite";
    }

    public static GW2 getInstance() {
        if (GW2.instance == null) {
            GW2.instance = new GW2("de");
        }
        return GW2.instance;
    }

    public static GW2 getInstance(String lang) {
        if (GW2.instance == null) {
            GW2.instance = new GW2(lang);
        }
        return GW2.instance;
    }

    public static String getBuild() throws IOException {
        JsonObject obj = jsonRequest(base_url + "/v1/build.json");
        return obj.get("build_id").getAsString();
    }

    public static ArrayList<String> getAllItemIds() throws IOException {
        JsonObject obj = jsonRequest(base_url + "/v1/items.json");
        JsonArray arr = obj.getAsJsonArray("items");
        ArrayList<String> results = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++) {
            results.add(arr.get(i).getAsString());
        }
        System.out.println("Got all item_id's (" + arr.size() + ")");
        return results;
    }

    public static ArrayList<String> getAllRecipeIds() throws IOException {
        JsonObject obj = jsonRequest(base_url + "/v1/recipes.json");
        JsonArray arr = obj.getAsJsonArray("recipes");
        ArrayList<String> results = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++) {
            results.add(arr.get(i).getAsString());
        }
        System.out.println("Got all recipe_id's (" + arr.size() + ")");
        return results;
    }

    public static void getAllRecipes(Integer count) throws IOException, InterruptedException {
        final Integer threadCount = count;
        GW2.getInstance().runningThreads = 0;
        Integer timeCount = 0;
        ArrayList<String> ids = getAllRecipeIds();
        maxItems = ids.size();
        Integer singleThreadActions = Math.floorDiv(maxItems, threadCount);
        Integer rest = maxItems - (threadCount * singleThreadActions);

        ArrayList<GW2RecipeRequestThread> threads = new ArrayList<>();
        for (int i = 0; i < threadCount; i++) {
            if (i == threadCount - 1) {
                threads.add(new GW2RecipeRequestThread(ids.subList((i * singleThreadActions) + 1, maxItems), i));
            } else {
                threads.add(new GW2RecipeRequestThread(ids.subList(i * singleThreadActions, (i + 1) * singleThreadActions), i));
            }

            threads.get(i).start();
            GW2.getInstance().runningThreads++;
        }
        System.out.println("ALL Recipe THREADS STARTED");
        while (GW2.getInstance().runningThreads > 0) {
            Thread.sleep(1000);
            timeCount++;
        }
        System.out.println("DONE!!!!!!");
        System.out.println("duration: " + (timeCount) + " sek");
        for (int i = 0; i < threadCount; i++) {
            threads.get(i).stop();
        }
    }

    public static void getAllItems(Integer count) throws IOException, InterruptedException {
        final Integer threadCount = count;
        GW2.getInstance().runningThreads = 0;
        Integer timeCount = 0;
        ArrayList<String> ids = getAllItemIds();
        maxItems = ids.size();
        Integer singleThreadActions = Math.floorDiv(maxItems, threadCount);
        Integer rest = maxItems - (threadCount * singleThreadActions);

        ArrayList<GW2ItemRequestThread> threads = new ArrayList<>();
        //ArrayList<List<String>> parts = new ArrayList<>();
        for (int i = 0; i < threadCount; i++) {
            if (i == threadCount - 1) {
                threads.add(new GW2ItemRequestThread(ids.subList((i * singleThreadActions) + 1, maxItems), i));
            } else {
                threads.add(new GW2ItemRequestThread(ids.subList(i * singleThreadActions, (i + 1) * singleThreadActions), i));
            }

            threads.get(i).start();
            GW2.getInstance().runningThreads++;
        }
        System.out.println("ALL ITEM THREADS STARTED");
        while (GW2.getInstance().runningThreads > 0) {
            Thread.sleep(1000);
            timeCount++;
        }
        System.out.println("DONE!!!!!!");
        System.out.println("duration: " + (timeCount) + " sek");
        for (int i = 0; i < threadCount; i++) {
            threads.get(i).stop();
        }
    }

    public synchronized void signalFinishedThread() {
        GW2.getInstance().runningThreads--;
    }

    public synchronized void addThreadResult(GW2Item elem) {
        allItems.add(elem);
        if ((allItems.size() % 100) == 0) {
            System.out.println("TOTAL ELEMENTS: " + allItems.size());
        }
    }

    public synchronized void addThreadResult(GW2Recipe elem) {
        allRecipes.add(elem);
        if ((allRecipes.size() % 100) == 0) {
            System.out.println("TOTAL ELEMENTS: " + allRecipes.size());
        }
    }

    public static void addToLocalDatabase(Integer typeID) throws IOException, Exception {
        if (!sql.isConnected()) {
            sql.openDB(db_path);
        }
        switch (typeID) {
            case 1:
                sql.addGW2ItemList(allItems);
                break;
            case 2:
                sql.addGW2RecipeList(allRecipes);
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
        }

    }

    public static GW2Item getItem(String item_id) throws IOException, Exception {
        JsonObject obj = jsonRequest(base_url + "/v1/item_details.json?item_id=" + item_id + "&lang=" + langCode);
        GW2Item result = new GW2Item(obj);
        return result;
    }

    public static GW2Recipe getRecipe(String recipe_id) throws IOException {
        JsonObject obj = jsonRequest(base_url + "/v1/recipe_details.json?recipe_id=" + recipe_id);
        GW2Recipe result = new GW2Recipe(obj);
        return result;
    }

    private static JsonObject jsonRequest(String url) throws MalformedURLException, IOException {
        URL src_url = new URL(url);
        HttpURLConnection request = (HttpURLConnection) src_url.openConnection();
        request.connect();
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        JsonObject result = root.getAsJsonObject();
        return result;
    }

    public static ArrayList<GW2Item> searchItem(String[] attr, String[] values, Boolean directSearch) throws Exception {
        if (usingLocalData) {
            if (!sql.isConnected()) {
                sql.openDB(db_path);
            }
            ArrayList<GW2Item> result = sql.queryItems(attr, values, directSearch);
            return result;
        }
        return null;
    }

    public static void useLocalDatabase(Boolean use) {
        if (use) {
            File test = new File(System.getProperty("user.dir") + "\\databases\\gw2.sqlite");
            if (test.exists()) {
                usingLocalData = use;
            }
        }
    }
}
