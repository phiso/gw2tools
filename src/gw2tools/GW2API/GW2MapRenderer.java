/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gw2tools.GW2API;

import java.util.ArrayList;
import javafx.scene.image.Image;

/**
 *
 * @author Philipp
 */
public class GW2MapRenderer {

    private static ArrayList<ArrayList<Image>> mapTiles;
    private static String base_render_url = "https://tiles.guildwars2.com/1/1/z/x/y.jpg";
    private static GW2MapRenderer instance;

    private GW2MapRenderer() {
        mapTiles = new ArrayList<>();
        ArrayList<Image> yArr = new ArrayList<>();
        for (Integer x = 0; x < 128; x++) {
            for (Integer y = 0; y < 128; y++) {
                String url = base_render_url.replace("z", "7").replace("x", x.toString()).replace("y", y.toString());               
                Image temp = new Image(url);
                yArr.add(temp);
                System.out.println("rendered "+y+" tiles from 128");
            }
            mapTiles.add(yArr);
        }
        System.out.println("all parts rendered");
    }

    public static GW2MapRenderer getInstance() {
        if (GW2MapRenderer.instance == null) {
            GW2MapRenderer.instance = new GW2MapRenderer();
        }
        return GW2MapRenderer.instance;
    }
}
