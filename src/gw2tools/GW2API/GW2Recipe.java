/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gw2tools.GW2API;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Philipp
 */
public class GW2Recipe {

    private String recipe_id;
    private String recipe_type;
    private String recipe_output_item;
    private String recipe_output_item_count;
    private String recipe_minRating;
    private String recipe_craftTime;
    private SimpleStringProperty recipeID;
    private SimpleStringProperty recipeName;

    //public final String recipe_vendor_value;
    private ArrayList<String> recipe_disciplines;
    private ArrayList<String> recipe_flags;
    private HashMap<String, String> recipe_ingredients;

    public GW2Recipe() {
        recipe_disciplines = new ArrayList<>();
        recipe_flags = new ArrayList<>();
        recipe_ingredients = new HashMap<>();

        recipe_id = "";
        recipe_type = "";
        recipe_output_item = "";
        recipe_output_item_count = "";
        recipe_minRating = "";
        recipe_craftTime = "";
    }

    public GW2Recipe(JsonObject obj) {
        recipe_disciplines = new ArrayList<>();
        recipe_flags = new ArrayList<>();
        recipe_ingredients = new HashMap<>();

        recipe_id = obj.get("recipe_id").getAsString();
        recipe_type = obj.get("type").getAsString();
        recipe_output_item = obj.get("output_item_id").getAsString();
        recipe_output_item_count = obj.get("output_item_count").getAsString();
        recipe_minRating = obj.get("min_rating").getAsString();
        recipe_craftTime = obj.get("time_to_craft_ms").getAsString();

        JsonArray dis = obj.getAsJsonArray("disciplines");
        for (int i = 0; i < dis.size(); i++) {
            recipe_disciplines.add(dis.get(i).getAsString());
        }
        
        JsonArray ingredients = obj.getAsJsonArray("ingredients");
        for (int i = 0; i < ingredients.size(); i++) {
            JsonObject temp = ingredients.get(i).getAsJsonObject();
            recipe_ingredients.put(temp.get("item_id").getAsString(), temp.get("count").getAsString());
        }
    }

    public String getRecipe_id() {
        return recipe_id;
    }

    public String getRecipe_type() {
        return recipe_type;
    }

    public String getRecipe_output_item() {
        return recipe_output_item;
    }

    public String getRecipe_output_item_count() {
        return recipe_output_item_count;
    }

    public String getRecipe_minRating() {
        return recipe_minRating;
    }

    public String getRecipe_craftTime() {
        return recipe_craftTime;
    }

    public ArrayList<String> getRecipe_disciplines() {
        return recipe_disciplines;
    }

    public ArrayList<String> getRecipe_flags() {
        return recipe_flags;
    }

    public HashMap<String, String> getRecipe_ingredients() {
        return recipe_ingredients;
    }

    public void setRecipe_id(String recipe_id) {
        this.recipe_id = recipe_id;
    }

    public void setRecipe_type(String recipe_type) {
        this.recipe_type = recipe_type;
    }

    public void setRecipe_output_item(String recipe_output_item) {
        this.recipe_output_item = recipe_output_item;
    }

    public void setRecipe_output_item_count(String recipe_output_item_count) {
        this.recipe_output_item_count = recipe_output_item_count;
    }

    public void setRecipe_minRating(String recipe_minRating) {
        this.recipe_minRating = recipe_minRating;
    }

    public void setRecipe_craftTime(String recipe_craftTime) {
        this.recipe_craftTime = recipe_craftTime;
    }

    public void setRecipe_disciplines(ArrayList<String> recipe_disciplines) {
        this.recipe_disciplines = recipe_disciplines;
    }

    public void setRecipe_flags(ArrayList<String> recipe_flags) {
        this.recipe_flags = recipe_flags;
    }

    public void setRecipe_ingredients(HashMap<String, String> recipe_ingredients) {
        this.recipe_ingredients = recipe_ingredients;
    }

    public SimpleStringProperty getRecipeIDProperty() {
        return recipeID;
    }

    public SimpleStringProperty getRecipeNameProperty() {
        return recipeName;
    }

    public String getRecipeID() {
        return recipeID.get();
    }

    public String getRecipeName() {
        return recipeName.get();
    }
}
