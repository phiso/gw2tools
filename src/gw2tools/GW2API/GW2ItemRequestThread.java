/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gw2tools.GW2API;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Philipp
 */
public class GW2ItemRequestThread extends Thread {

    private List<String> ids;
    private Integer threadId;
    private GW2Item temp_item = null;   

    public GW2ItemRequestThread(List<String> ids, Integer id) {
        this.ids = ids;
        threadId = id;
    }

    private GW2Item getItem(String id) throws Exception {
        try {
            GW2Item item = GW2.getItem(id);
            return item;
        } catch (IOException ex) {
            return this.getItem(id);
        }
    }

    @Override
    public void run() {
        for (String elem : ids) {
            try {
                temp_item = getItem(elem);
            } catch (Exception ex) {
                Logger.getLogger(GW2ItemRequestThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            GW2.getInstance().addThreadResult(temp_item);
        }
        GW2.getInstance().signalFinishedThread();        
    }

    private static JsonObject jsonRequest(String url) throws MalformedURLException, IOException {
        URL src_url = new URL(url);
        HttpURLConnection request = (HttpURLConnection) src_url.openConnection();
        request.connect();
        JsonParser jp = new JsonParser();
        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
        JsonObject result = root.getAsJsonObject();
        return result;
    }
}
